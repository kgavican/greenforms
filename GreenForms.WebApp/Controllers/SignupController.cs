﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Exceptions;
using GreenForms.AppCore.Interfaces;
using GreenForms.WebApp.Models.Signup;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GreenForms.WebApp.Controllers
{
    public class SignupController : Controller
    {
        readonly INewsletterSignupService _newsletterSignupService;

        public SignupController(INewsletterSignupService newsletterSignupService){
            _newsletterSignupService = newsletterSignupService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var viewModel = new NewsletterSignupFormViewModel()
            {
                HowDidYouHearAboutUsList = await GetHearAboutSources()
            };

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Index(NewsletterSignupFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var signup = new NewsletterSignup(viewModel.EmailAddress, viewModel.HowDidYouHearAboutUsId,
                    viewModel.ReasonForSignup);

                try
                {
                    await _newsletterSignupService.CreateNewsletterSigupAsync(signup);

                    return View("ThankYou");
                }
                catch (EmailAlreadyExistsException)
                {
                    ModelState.AddModelError("EmailAddress", "Already signed up");
                }
                catch (Exception ex)
                {
                    // log ex
                    // do a friendly error
                }
            }

            viewModel.HowDidYouHearAboutUsList = await GetHearAboutSources();

            return View(viewModel);
        }

        // probabaly should move this to a builder or service, but just leaving here for now
        private async Task<IEnumerable<SelectListItem>> GetHearAboutSources()
        {
            var sources = await _newsletterSignupService.GetNewsletterSignupSources();

            var items = new List<SelectListItem>();
            foreach (var source in sources)
            {
                items.Add(new SelectListItem() { Value = source.Id.ToString(), Text = source.Source });
            }

            return items;
        }

    }
}
