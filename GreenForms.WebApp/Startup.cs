﻿using GreenForms.AppCore.Interfaces;
using GreenForms.AppCore.Services;
using GreenForms.Infrastructure.Data;
using GreenForms.Infrastructure.Data.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GreenForms.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<GreenFormsContext>(x => 
                x.UseSqlServer(Configuration.GetConnectionString("SqlConnection")));

            services.AddMvc();

            services.AddScoped(typeof(IAsyncRepository<>), typeof(EntityRepository<>));

            services.AddScoped<INewsletterSignupRepo, NewsletterSignupRepo>();

            services.AddScoped<INewsletterSignupService, NewsletterSignupService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Signup/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Signup}/{action=Index}/{id?}");
            });
        }
    }
}
