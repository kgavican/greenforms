﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GreenForms.WebApp.Models.Signup
{
    public class NewsletterSignupFormViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        public IEnumerable<SelectListItem> HowDidYouHearAboutUsList { get; set; }

        [BindRequired]
        [Range(1, Int32.MaxValue, ErrorMessage = "The field How did you hear about us? is required")]
        [Display(Name = "How did you hear about us?")]
        public int HowDidYouHearAboutUsId { get; set; }

        [Display(Name = "Why are you are signing up?")]
        public string ReasonForSignup { get; set; }
    }
}
