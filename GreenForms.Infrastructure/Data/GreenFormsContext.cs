﻿using GreenForms.AppCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreenForms.Infrastructure.Data
{
    public class GreenFormsContext : DbContext
    {
        public DbSet<NewsletterSignup> NewsletterSignups { get; set; }
        public DbSet<HearAboutSource> HearAboutSources { get; set; }

        public GreenFormsContext(DbContextOptions<GreenFormsContext> options) : base(options)
        {     
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<NewsletterSignup>(ConfigNewsletterSignups);
            builder.Entity<HearAboutSource>(ConfigHearAboutSources);
        }

        private void ConfigNewsletterSignups(EntityTypeBuilder<NewsletterSignup> builder)
        {
            builder.HasKey(i => i.Id);

            builder.Property(i => i.Id)
                   .IsRequired(true);

            builder.HasOne(i => i.HearAboutSource)
                   .WithMany()
                   .HasForeignKey(ci => ci.HearAboutSourceId);

            builder.Property(i => i.ReasonForSignup)
                   .IsRequired(false);
        }

        private void ConfigHearAboutSources(EntityTypeBuilder<HearAboutSource> builder)
        {
            builder.HasKey(i => i.Id);

            builder.Property(i => i.Id)
                   .IsRequired(true);

            builder.Property(i => i.Source)
                   .IsRequired(true);
        }


    }
}
