﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GreenForms.Infrastructure.Data.Repository
{
    public class EntityRepository<T> : IAsyncRepository<T> where T : BaseEntity
    {
        protected readonly GreenFormsContext _context;

        public EntityRepository(GreenFormsContext context)
        {
            _context = context;
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);

            await _context.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public virtual async Task<List<T>> ListAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }
    }
}
