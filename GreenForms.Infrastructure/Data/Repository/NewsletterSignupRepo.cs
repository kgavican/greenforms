﻿using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GreenForms.Infrastructure.Data.Repository
{
    public class NewsletterSignupRepo : EntityRepository<NewsletterSignup>, INewsletterSignupRepo
    {
        public NewsletterSignupRepo(GreenFormsContext context): base (context){          
        }

        public async Task<NewsletterSignup> GetByEmailAsync(string emailAddress)
        {
            return await _context.NewsletterSignups
                          .FirstOrDefaultAsync(x => x.EmailAddress == emailAddress);
        }
    }
}
