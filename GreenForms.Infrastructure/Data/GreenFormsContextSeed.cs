﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;

namespace GreenForms.Infrastructure.Data
{
    public class GreenFormsContextSeed
    {
        public static async Task SeedAsync(GreenFormsContext context)
        {
            if (!context.HearAboutSources.Any())
            {
                var defaults = new List<HearAboutSource>()
                {
                    new HearAboutSource() { Source = "Advert"},
                    new HearAboutSource() { Source = "Word of Mouth" },
                    new HearAboutSource() { Source = "Other" },
                };

                context.HearAboutSources.AddRange(defaults);

                await context.SaveChangesAsync();
            }
        }
    }
}
