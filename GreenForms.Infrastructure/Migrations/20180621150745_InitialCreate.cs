﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GreenForms.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HearAboutSources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Source = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HearAboutSources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NewsletterSignups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmailAddress = table.Column<string>(nullable: true),
                    HearAboutSourceId = table.Column<int>(nullable: false),
                    ReasonForSignup = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsletterSignups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewsletterSignups_HearAboutSources_HearAboutSourceId",
                        column: x => x.HearAboutSourceId,
                        principalTable: "HearAboutSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NewsletterSignups_HearAboutSourceId",
                table: "NewsletterSignups",
                column: "HearAboutSourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewsletterSignups");

            migrationBuilder.DropTable(
                name: "HearAboutSources");
        }
    }
}
