﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Exceptions;
using GreenForms.AppCore.Interfaces;

namespace GreenForms.AppCore.Services
{
    public class NewsletterSignupService : INewsletterSignupService
    {
        private readonly INewsletterSignupRepo _signupRepo;

        private readonly IAsyncRepository<HearAboutSource> _hearaboutSourceRepo;

        public NewsletterSignupService(INewsletterSignupRepo signupRepo, IAsyncRepository<HearAboutSource> hearaboutSourceRepo)
        {
            _signupRepo = signupRepo;
            _hearaboutSourceRepo = hearaboutSourceRepo;
        }

        public async Task CreateNewsletterSigupAsync(NewsletterSignup newsletterSignup)
        {
            var existing = await _signupRepo.GetByEmailAsync(newsletterSignup.EmailAddress);

            // Business rule : only allow one sign up per email
            if (existing != null) throw new EmailAlreadyExistsException();

            await _signupRepo.AddAsync(newsletterSignup);
        }

        public async Task<List<HearAboutSource>> GetNewsletterSignupSources()
        {
            return await _hearaboutSourceRepo.ListAllAsync();
        }
    }
}
