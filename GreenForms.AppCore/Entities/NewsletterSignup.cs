﻿namespace GreenForms.AppCore.Entities
{
    public class NewsletterSignup : BaseEntity
    {
        public NewsletterSignup(string emailAddress, int hearAboutSourceId, string reasonForSignup = null){
            EmailAddress = emailAddress.ToLower();
            HearAboutSourceId = hearAboutSourceId;
            ReasonForSignup = reasonForSignup;
        }

        public string EmailAddress { get; set; }

        public int HearAboutSourceId { get; set; }

        public HearAboutSource HearAboutSource { get; set; }

        public string ReasonForSignup { get; set; }

    }
}
