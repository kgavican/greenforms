﻿namespace GreenForms.AppCore.Entities
{
    public class BaseEntity
    {
        // All entities stored with Id
        public int Id { get; set; }
    }
}
