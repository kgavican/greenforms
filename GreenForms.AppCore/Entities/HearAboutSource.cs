﻿namespace GreenForms.AppCore.Entities
{
    public class HearAboutSource : BaseEntity
    {
        public string Source { get; set; }
    }
}
