﻿using System.Threading.Tasks;
using GreenForms.AppCore.Entities;

namespace GreenForms.AppCore.Interfaces
{
    public interface INewsletterSignupRepo : IAsyncRepository<NewsletterSignup>
    {
        Task<NewsletterSignup> GetByEmailAsync(string emailAddress);
    }
}
