﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;

namespace GreenForms.AppCore.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);

        Task<T> AddAsync(T entity);

        Task<List<T>> ListAllAsync();
    }
}
