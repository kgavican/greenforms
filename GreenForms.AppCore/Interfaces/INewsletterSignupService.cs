﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;

namespace GreenForms.AppCore.Interfaces
{
    public interface INewsletterSignupService
    {
        Task CreateNewsletterSigupAsync(NewsletterSignup newsletterSignup);

        Task<List<HearAboutSource>> GetNewsletterSignupSources();
    }
}
