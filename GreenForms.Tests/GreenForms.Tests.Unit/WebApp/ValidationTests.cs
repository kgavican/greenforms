﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GreenForms.WebApp.Models.Signup;
using Xunit;

namespace GreenForms.Tests.Unit.WebApp
{
    public class ValidationTests
    {
        [Fact]
        public void ValidateSignupModelHearAboutSource()
        {
            // Arrange
            var viewModel = new NewsletterSignupFormViewModel()
            {
                EmailAddress = "test@emial.com"
            };

            var context = new ValidationContext(viewModel, null, null);
            var results = new List<ValidationResult>();

            // Act
            var isValidateObject = Validator.TryValidateObject(viewModel, context, results, true);

            // Assert
            Assert.False(isValidateObject);
            Assert.True(results.Count == 1);
            Assert.Contains("HowDidYouHearAboutUsId", results[0].MemberNames);
        }

        [Fact]
        public void ValidateSignupModelEmailAddress()
        {
            // Arrange
            var viewModel = new NewsletterSignupFormViewModel()
            {
                EmailAddress = "test.com",
                HowDidYouHearAboutUsId = 1
            };

            var context = new ValidationContext(viewModel, null, null);
            var results = new List<ValidationResult>();

            // Act
            var isValidateObject = Validator.TryValidateObject(viewModel, context, results, true);

            // Assert
            Assert.False(isValidateObject);
            Assert.True(results.Count == 1);
            Assert.Contains("EmailAddress", results[0].MemberNames);
        }
    }
}
