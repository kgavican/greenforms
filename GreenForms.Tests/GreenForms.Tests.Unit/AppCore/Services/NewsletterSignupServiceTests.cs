﻿using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Exceptions;
using GreenForms.AppCore.Interfaces;
using GreenForms.AppCore.Services;
using Xunit;
using Moq;

namespace GreenForms.Tests.Unit.AppCore.Services
{
    public class NewsletterSignupServiceTests
    {
        private readonly Mock<INewsletterSignupRepo> _signupRepo;
        private readonly Mock<IAsyncRepository<HearAboutSource>> _hearaboutSourceRepo;
        
        public NewsletterSignupServiceTests()
        {
            _hearaboutSourceRepo = new Mock<IAsyncRepository<HearAboutSource>>();

            _signupRepo = new Mock<INewsletterSignupRepo>();

            _signupRepo.Setup(i => i.GetByEmailAsync("test@test.com")).Returns(Task.FromResult(new NewsletterSignup("test@test.com", 1)));
        }


        [Fact]
        public async Task ThrowExceptionForExistingEmailAddress()
        {
            // Arrange
            var service = new NewsletterSignupService(_signupRepo.Object, _hearaboutSourceRepo.Object);

            // Act & Assert 
            await Assert.ThrowsAsync<EmailAlreadyExistsException>(async () => await service.CreateNewsletterSigupAsync(new NewsletterSignup("test@test.com", 3)));

            // Assert some more
            _signupRepo.Verify(x => x.AddAsync(It.IsAny<NewsletterSignup>()), Times.Never);
        }

        [Fact]
        public async Task AddForNewEmailAddress()
        {
            // Arrange
            var service = new NewsletterSignupService(_signupRepo.Object, _hearaboutSourceRepo.Object);

            // Act 
            await service.CreateNewsletterSigupAsync(new NewsletterSignup("testOther@test.com", 1));

            // Assert 
            _signupRepo.Verify(x => x.AddAsync(It.IsAny<NewsletterSignup>()), Times.Once());
        }
    }
}
