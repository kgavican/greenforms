﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreenForms.AppCore.Entities;
using GreenForms.AppCore.Services;
using GreenForms.Infrastructure.Data;
using GreenForms.Infrastructure.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace GreenForms.Tests.Integration.AppCore.Services
{
    public class NewsletterSignupServiceTests
    {
        // Use a real database to test the service, but in memoery

        DbContextOptions<GreenFormsContext> options = new DbContextOptionsBuilder<GreenFormsContext>().UseInMemoryDatabase().Options;

        [Fact]
        public async Task ValidNewsletterSignupIsSaved()
        {
            // Arrange
            // Given a valid signup
            var validSignup = new NewsletterSignup("test@test.com", 2, "Testing testing");

            // Act
            // When I save the signup
            using (var context = new GreenFormsContext(options))
            {
                await GreenFormsContextSeed.SeedAsync(context);

                var signupService = new NewsletterSignupService(new NewsletterSignupRepo(context), new EntityRepository<HearAboutSource>(context));

                await signupService.CreateNewsletterSigupAsync(validSignup);
            }

            // Arrange
            // Then it is saved to the database as expected
            using (var context = new GreenFormsContext(options))
            {
                var savedEntry = context.NewsletterSignups.Include(i => i.HearAboutSource).FirstOrDefault();

                Assert.NotNull(savedEntry);
                Assert.Equal("test@test.com", savedEntry.EmailAddress);
                Assert.Equal("Word of Mouth", savedEntry.HearAboutSource.Source);
                Assert.Equal("Testing testing", savedEntry.ReasonForSignup);
            }
        }

    }
}
